---
title: "Les résultats statistique sur les données RMQS"

description: "Analyse statistique"
cascade:
  featured_image: '/images/sol.jpg'

---

Le Réseau de mesures de la qualité des sols est un programme du Groupement d’intérêt scientifique (GIS) Sol. Ce groupement réunit des représentants des ministères en charge de l’agriculture et de l’environnement, de l’Agence de l’environnement et de la maîtrise de l’énergie (ADEME), de l’Institut national de la recherche agronomique (INRA), de l’Institut de recherche pour le développement (IRD) et de l’Institut national de l'information géographique et forestière (IGN).